

let showIn3D = false;


function createStatusPanels() 
{
  let topStatusPanel = createP("");
  topStatusPanel.id("topStatusPanel");

  let middleStatusPanel = createP("");
  middleStatusPanel.id("middleStatusPanel");

  let bottomStatusPanel = createP("TRANSPARENT PERCEPTRON");
  bottomStatusPanel.id("bottomStatusPanel");
}


function updateTopStatusPanel (status) 
{
  document.getElementById("topStatusPanel").innerHTML = status;
}


function updateMiddleStatusPanel (status) 
{
  document.getElementById("middleStatusPanel").innerHTML = status;
}


function switch2D3D()
{
  showIn3D = !showIn3D;
}